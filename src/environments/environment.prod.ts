export const environment = {
  production: true,
  apiUrl: 'https://www.banjohealth.com/_api/',
  env: 'prod',
};
