import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { CustomerBodyComponent } from './customers/customer-body/customer-body.component';
import { SoulutionsBodyComponent } from './solutions/soulutions-body/soulutions-body.component';
import { CompanyBodyComponent } from './company/company-body/company-body.component';
import { BlogBodyComponent } from './blog/blog-body/blog-body.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
  {
    path: 'solutions',
    component: SoulutionsBodyComponent,
  },
  {
    path: 'company',
    component: CompanyBodyComponent,
  },
  {
    path: 'customers',
    component: CustomerBodyComponent,
  },
  {
    path: 'blog',
    component: BlogBodyComponent,
  },
  // How you would apply guards to protect logins
  // {
  //   path: '',
  //   // canActivate: [AuthGuard],
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
