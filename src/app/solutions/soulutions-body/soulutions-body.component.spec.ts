import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoulutionsBodyComponent } from './soulutions-body.component';

describe('SoulutionsBodyComponent', () => {
  let component: SoulutionsBodyComponent;
  let fixture: ComponentFixture<SoulutionsBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoulutionsBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoulutionsBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
