import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-company-body',
  templateUrl: './company-body.component.html',
  styleUrls: ['./company-body.component.scss'],
})
export class CompanyBodyComponent implements OnInit {
  private titleText = 'Company';
  constructor(private titleSvc: TitleService) {}

  ngOnInit(): void {
    this.titleSvc.setTitle(this.titleText);
  }
}
