import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBodyComponent } from './company-body.component';

describe('CompanyBodyComponent', () => {
  let component: CompanyBodyComponent;
  let fixture: ComponentFixture<CompanyBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
