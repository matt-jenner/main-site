import { Component, OnInit } from '@angular/core';
import { TitleService } from '../services/title.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  private titleText = 'AI-Powered Prior Authorization';
  constructor(private titleSvc: TitleService) {}

  ngOnInit(): void {
    this.titleSvc.setTitle(this.titleText);
  }
}
