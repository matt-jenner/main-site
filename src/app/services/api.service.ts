import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { ApiResponse } from '../models/api-response.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}
  // This is where we would access the api methods
  getData(dataId: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(
      `${environment.apiUrl}data/${dataId}`,
      {}
    );
  }
}
