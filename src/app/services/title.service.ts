import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class TitleService {
  constructor(private docTitle: Title) {}

  setTitle(text: string): void {
    this.docTitle.setTitle(`${text} | Banjo Health`);
  }
}
