// Angular packages and components
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Services
import { ApiService } from './services/api.service';
import { TitleService } from './services/title.service';

// Added components
import { BlogBodyComponent } from './blog/blog-body/blog-body.component';
import { BlogDetailComponent } from './blog/blog-detail/blog-detail.component';
import { StoryComponent } from './company/story/story.component';
import { TeamComponent } from './company/team/team.component';
import { TeamDetailComponent } from './company/team/team-detail/team-detail.component';
import { CareersComponent } from './company/careers/careers.component';
import { SolutionDetailComponent } from './solutions/solution-detail/solution-detail.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SoulutionsBodyComponent } from './solutions/soulutions-body/soulutions-body.component';
import { CustomerBodyComponent } from './customers/customer-body/customer-body.component';
import { CompanyBodyComponent } from './company/company-body/company-body.component';
import { LandingPageComponent } from './landing-page/landing-page.component';

// Kendo controls
import { NavigationModule } from '@progress/kendo-angular-navigation';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { IndicatorsModule } from '@progress/kendo-angular-indicators';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { IconsModule } from '@progress/kendo-angular-icons';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { LabelModule } from '@progress/kendo-angular-label';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ButtonGroupModule } from '@progress/kendo-angular-buttons';
import { MenusModule } from '@progress/kendo-angular-menu';
import { TextBoxModule } from '@progress/kendo-angular-inputs';

const kendoControls = [
  NavigationModule,
  LayoutModule,
  IndicatorsModule,
  InputsModule,
  IconsModule,
  ButtonsModule,
  LabelModule,
  DropDownsModule,
  PopupModule,
  ButtonGroupModule,
  MenusModule,
  TextBoxModule,
];

@NgModule({
  declarations: [
    AppComponent,
    BlogBodyComponent,
    BlogDetailComponent,
    CustomerBodyComponent,
    CompanyBodyComponent,
    StoryComponent,
    TeamComponent,
    TeamDetailComponent,
    CareersComponent,
    SoulutionsBodyComponent,
    SolutionDetailComponent,
    HeaderComponent,
    FooterComponent,
    LandingPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NavigationModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ...kendoControls,
  ],
  providers: [ApiService, TitleService],
  bootstrap: [AppComponent],
})
export class AppModule {}
