import { Component, OnInit } from '@angular/core';
import { linkedinIcon, twitterIcon } from '@progress/kendo-svg-icons';
import { Router } from '@angular/router';
import { TextBoxComponent } from '@progress/kendo-angular-inputs';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  public icons = { linkedin: linkedinIcon, twitter: twitterIcon };
  public email = '';
  public year = new Date().getFullYear();
  constructor(public router: Router) {}

  ngOnInit(): void {}
}
