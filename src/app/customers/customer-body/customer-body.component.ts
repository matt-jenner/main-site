import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-customer-body',
  templateUrl: './customer-body.component.html',
  styleUrls: ['./customer-body.component.scss'],
})
export class CustomerBodyComponent implements OnInit {
  private titleText = 'Customers';
  constructor(private titleSvc: TitleService) {}

  ngOnInit(): void {
    this.titleSvc.setTitle(this.titleText);
  }
}
